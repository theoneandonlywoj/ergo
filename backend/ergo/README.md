# Ergo

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Docker Setup
### Build
```bash
sudo docker build \
--build-arg DB_USERNAME=postgres \
--build-arg DB_PASSWORD=password \
--build-arg DB_HOSTNAME=whatisfortea.cq9n4g08jpqg.eu-west-2.rds.amazonaws.com \
--build-arg SECRET_KEY_BASE=YAVOl4ZXMbmHcjRKdd6EN+qDejHZfM1C9KbLB3mXG7P+IhhjW1YRIEWkLfFvAFjx \
-t ergo_backend . 
```

### Run interactively
```bash
sudo docker run -it -p 4000:80 --rm ergo_backend
```

### Run production
```bash
sudo docker run -p 4000:80 -d ergo_backend
```

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
