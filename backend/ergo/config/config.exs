# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ergo,
  ecto_repos: [Ergo.Repo]

# Configures the endpoint
config :ergo, ErgoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "h4SLlcd5rPxyD4mikLcgJl+OZiSYdSWtnR8MeXqSi0KDFrwA371UoU03odliyHNz",
  render_errors: [view: ErgoWeb.ErrorView, accepts: ~w(json)],
  pubsub_server: Ergo.PubSub

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
