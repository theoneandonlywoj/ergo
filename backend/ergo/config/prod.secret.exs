# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
use Mix.Config

database_username =
  System.get_env("DB_USERNAME") ||
  raise """
  environment variable DB_USERNAME is missing.
  """

database_password =
  System.get_env("DB_PASSWORD") ||
  raise """
  environment variable DB_PASSWORD is missing.
  """

database_hostname =
  System.get_env("DB_HOSTNAME") ||
  raise """
  environment variable DB_HOSTNAME is missing.
  """

config :ergo, Ergo.Repo,
  username: database_username,
  password: database_password,
  database: "ergo_prod",
  hostname: database_hostname,
  show_sensitive_data_on_connection_error: false,
  pool_size: 10

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    "YAVOl4ZXMbmHcjRKdd6EN+qDejHZfM1C9KbLB3mXG7P+IhhjW1YRIEWkLfFvAFjx" ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :ergo, ErgoWeb.Endpoint, server: true,
  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],
  secret_key_base: secret_key_base

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :ergo, ErgoWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
