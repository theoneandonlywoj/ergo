defmodule Ergo.Admin do
  @moduledoc """
  The Admin context.
  """

  import Ecto.Query, warn: false
  alias Ergo.Repo

  alias Ergo.Admin.Cohort
  alias Ergo.Admin.UserCohortSubscription
  alias Ergo.CustomHelpers

  @doc """
  Returns the list of cohorts.

  ## Examples

      iex> list_cohorts()
      [%Cohort{}, ...]

  """
  def list_cohorts do
    Repo.all(Cohort)
  end

  def list_cohorts_for_user_subscriptions(user_id) do
    user_cohort_subs =
      from s in UserCohortSubscription,
        where: s.user_id == ^user_id

    cohorts_for_users =
      from c in Cohort,
        join: s in subquery(user_cohort_subs),
        on: c.id == s.cohort_id,
        order_by: [asc: c.name]

    cohorts_for_users |> Repo.all()
  end

  @doc """
  Gets a single cohort.

  Raises `Ecto.NoResultsError` if the Cohort does not exist.

  ## Examples

      iex> get_cohort!(123)
      %Cohort{}

      iex> get_cohort!(456)
      ** (Ecto.NoResultsError)

  """
  def get_cohort!(id), do: Repo.get!(Cohort, id)

  @doc """
  Creates a cohort.

  ## Examples

      iex> create_cohort(%{field: value})
      {:ok, %Cohort{}}

      iex> create_cohort(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_cohort(attrs \\ %{}) do
    %Cohort{}
    |> Cohort.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a cohort.

  ## Examples

      iex> update_cohort(cohort, %{field: new_value})
      {:ok, %Cohort{}}

      iex> update_cohort(cohort, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_cohort(%Cohort{} = cohort, attrs) do
    cohort
    |> Cohort.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Cohort.

  ## Examples

      iex> delete_cohort(cohort)
      {:ok, %Cohort{}}

      iex> delete_cohort(cohort)
      {:error, %Ecto.Changeset{}}

  """
  def delete_cohort(%Cohort{} = cohort) do
    Repo.delete(cohort)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking cohort changes.

  ## Examples

      iex> change_cohort(cohort)
      %Ecto.Changeset{source: %Cohort{}}

  """
  def change_cohort(%Cohort{} = cohort) do
    Cohort.changeset(cohort, %{})
  end

  alias Ergo.Admin.Learner

  @doc """
  Returns the list of learners.

  ## Examples

      iex> list_learners()
      [%Learner{}, ...]

  """
  def list_learners do
    Repo.all(Learner)
  end

  def list_learners_for_cohort_id(cohort_id) do
    query = from l in Learner, where: l.cohort_id == ^cohort_id
    Repo.all(query)
  end

  @doc """
  Gets a single learner.

  Raises `Ecto.NoResultsError` if the Learner does not exist.

  ## Examples

      iex> get_learner!(123)
      %Learner{}

      iex> get_learner!(456)
      ** (Ecto.NoResultsError)

  """
  def get_learner!(id), do: Repo.get!(Learner, id)

  @doc """
  Creates a learner.

  ## Examples

      iex> create_learner(%{field: value})
      {:ok, %Learner{}}

      iex> create_learner(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_learner(attrs \\ %{}) do
    %Learner{}
    |> Learner.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a learner.

  ## Examples

      iex> update_learner(learner, %{field: new_value})
      {:ok, %Learner{}}

      iex> update_learner(learner, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_learner(%Learner{} = learner, attrs) do
    learner
    |> Learner.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Learner.

  ## Examples

      iex> delete_learner(learner)
      {:ok, %Learner{}}

      iex> delete_learner(learner)
      {:error, %Ecto.Changeset{}}

  """
  def delete_learner(%Learner{} = learner) do
    Repo.delete(learner)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking learner changes.

  ## Examples

      iex> change_learner(learner)
      %Ecto.Changeset{source: %Learner{}}

  """
  def change_learner(%Learner{} = learner) do
    Learner.changeset(learner, %{})
  end

  alias Ergo.Admin.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  def authenticate_user(email, password) do
    query =
      from(u in User,
        where: u.email == ^email
      )

    query
    |> Repo.one()
    |> verify_password(password)
  end

  defp verify_password(nil, _) do
    {:error, "User does not exist!"}
  end

  defp verify_password(user, password) do
    if user.password == password do
      {:ok, user}
    else
      {:error, "Wrong email or password!"}
    end
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Returns the list of user_cohort_subscriptions.

  ## Examples

      iex> list_user_cohort_subscriptions()
      [%UserCohortSubscription{}, ...]

  """
  def list_user_cohort_subscriptions do
    Repo.all(UserCohortSubscription)
  end

  @doc """
  Gets a single user_cohort_subscription.

  Raises `Ecto.NoResultsError` if the User cohort subscription does not exist.

  ## Examples

      iex> get_user_cohort_subscription!(123)
      %UserCohortSubscription{}

      iex> get_user_cohort_subscription!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user_cohort_subscription!(id), do: Repo.get!(UserCohortSubscription, id)

  @doc """
  Creates a user_cohort_subscription.

  ## Examples

      iex> create_user_cohort_subscription(%{field: value})
      {:ok, %UserCohortSubscription{}}

      iex> create_user_cohort_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_cohort_subscription(attrs \\ %{}) do
    %UserCohortSubscription{}
    |> UserCohortSubscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user_cohort_subscription.

  ## Examples

      iex> update_user_cohort_subscription(user_cohort_subscription, %{field: new_value})
      {:ok, %UserCohortSubscription{}}

      iex> update_user_cohort_subscription(user_cohort_subscription, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user_cohort_subscription(%UserCohortSubscription{} = user_cohort_subscription, attrs) do
    user_cohort_subscription
    |> UserCohortSubscription.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a UserCohortSubscription.

  ## Examples

      iex> delete_user_cohort_subscription(user_cohort_subscription)
      {:ok, %UserCohortSubscription{}}

      iex> delete_user_cohort_subscription(user_cohort_subscription)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_cohort_subscription(%UserCohortSubscription{} = user_cohort_subscription) do
    Repo.delete(user_cohort_subscription)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user_cohort_subscription changes.

  ## Examples

      iex> change_user_cohort_subscription(user_cohort_subscription)
      %Ecto.Changeset{source: %UserCohortSubscription{}}

  """
  def change_user_cohort_subscription(%UserCohortSubscription{} = user_cohort_subscription) do
    UserCohortSubscription.changeset(user_cohort_subscription, %{})
  end

  alias Ergo.Admin.LearnerEvent

  @doc """
  Returns the list of learner_events.

  ## Examples

      iex> list_learner_events()
      [%LearnerEvent{}, ...]

  """
  def list_learner_events(params) do
    LearnerEvent
    |> CustomHelpers.add_dynamic_filters_for_entity(params)
    |> Repo.all()
  end

  @doc """
  Gets a single learner_event.

  Raises `Ecto.NoResultsError` if the Learner event does not exist.

  ## Examples

      iex> get_learner_event!(123)
      %LearnerEvent{}

      iex> get_learner_event!(456)
      ** (Ecto.NoResultsError)

  """
  def get_learner_event!(id), do: Repo.get!(LearnerEvent, id)

  @doc """
  Creates a learner_event.

  ## Examples

      iex> create_learner_event(%{field: value})
      {:ok, %LearnerEvent{}}

      iex> create_learner_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_learner_event(attrs \\ %{}) do
    %LearnerEvent{}
    |> LearnerEvent.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a learner_event.

  ## Examples

      iex> update_learner_event(learner_event, %{field: new_value})
      {:ok, %LearnerEvent{}}

      iex> update_learner_event(learner_event, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_learner_event(%LearnerEvent{} = learner_event, attrs) do
    learner_event
    |> LearnerEvent.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a LearnerEvent.

  ## Examples

      iex> delete_learner_event(learner_event)
      {:ok, %LearnerEvent{}}

      iex> delete_learner_event(learner_event)
      {:error, %Ecto.Changeset{}}

  """
  def delete_learner_event(%LearnerEvent{} = learner_event) do
    Repo.delete(learner_event)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking learner_event changes.

  ## Examples

      iex> change_learner_event(learner_event)
      %Ecto.Changeset{source: %LearnerEvent{}}

  """
  def change_learner_event(%LearnerEvent{} = learner_event) do
    LearnerEvent.changeset(learner_event, %{})
  end
end
