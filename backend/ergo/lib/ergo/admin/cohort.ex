defmodule Ergo.Admin.Cohort do
  use Ecto.Schema
  import Ecto.Changeset

  schema "cohorts" do
    field :name, :string

    has_many :user_cohort_subscriptions, Ergo.Admin.UserCohortSubscription
    has_many :learners, Ergo.Admin.Learner

    timestamps()
  end

  @doc false
  def changeset(cohort, attrs) do
    cohort
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
