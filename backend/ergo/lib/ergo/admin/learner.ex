defmodule Ergo.Admin.Learner do
  use Ecto.Schema
  import Ecto.Changeset

  schema "learners" do
    # field :cohort_id, :integer
    field :name, :string
    field :rag_status, :string, default: "Green"

    belongs_to :cohort, Ergo.Admin.Cohort
    has_many :learner_events, Ergo.Admin.LearnerEvent
    has_many :marked_project, Ergo.Mentorship.MarkedProject

    timestamps()
  end

  @doc false
  def changeset(learner, attrs) do
    learner
    |> cast(attrs, [:name, :rag_status, :cohort_id])
    |> validate_required([:name, :cohort_id])
    |> assoc_constraint(:cohort)
  end
end
