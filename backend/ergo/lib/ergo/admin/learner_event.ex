defmodule Ergo.Admin.LearnerEvent do
  use Ecto.Schema
  import Ecto.Changeset

  schema "learner_events" do
    field :details, :string
    field :type, :string

    belongs_to :learner, Ergo.Admin.Learner

    timestamps()
  end

  @doc false
  def changeset(learner_event, attrs) do
    learner_event
    |> cast(attrs, [:learner_id, :type, :details])
    |> validate_required([:learner_id, :type, :details])
    |> assoc_constraint(:learner)
  end
end
