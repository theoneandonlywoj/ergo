defmodule Ergo.Admin.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, :string

    has_many :user_cohort_subscriptions, Ergo.Admin.UserCohortSubscription

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :password])
    |> validate_required([:email, :name, :password])
    |> unique_constraint(:email, name: :users_email_index)
  end
end
