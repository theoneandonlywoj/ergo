defmodule Ergo.Admin.UserCohortSubscription do
  use Ecto.Schema
  import Ecto.Changeset

  schema "user_cohort_subscriptions" do
    # field :cohort_id, :integer
    # field :user_id, :integer

    belongs_to :user, Ergo.Admin.User
    belongs_to :cohort, Ergo.Admin.Cohort

    timestamps()
  end

  @doc false
  def changeset(user_cohort_subscription, attrs) do
    user_cohort_subscription
    |> cast(attrs, [:user_id, :cohort_id])
    |> validate_required([:user_id, :cohort_id])
    |> assoc_constraint(:user)
    |> assoc_constraint(:cohort)
  end
end
