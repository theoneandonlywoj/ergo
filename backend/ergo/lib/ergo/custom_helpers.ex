defmodule Ergo.CustomHelpers do
  @moduledoc """
  Custom functions that do not belong in other places.
  """
  import Ecto.Query, warn: false

  def add_dynamic_filters_to_query(query, params_map) when params_map == %{} do
    query
  end

  def add_dynamic_filters_to_query(query, params_map) do
    [head | _tail] = Map.keys(params_map)
    value = Map.get(params_map, head)

    query = from q in query, where: field(q, ^head) == ^value

    params_map = Map.drop(params_map, [head])
    add_dynamic_filters_to_query(query, params_map)
  end

  def map_string_keys_to_map_atom_keys(map) do
    map
    |> Map.new(fn {k, v} -> {String.to_atom(k), v} end)
  end

  def add_dynamic_filters_for_entity(entity, params) do
    params_map = map_string_keys_to_map_atom_keys(params)

    entity
    |> add_dynamic_filters_to_query(params_map)
  end

  def replace_nill_with_empty_map(value) do
    case value do
      nil -> %{}
      _ -> value
    end
  end
end
