defmodule Ergo.DataUpload.LearnersSeeds do
  alias Ergo.Repo
  alias Ergo.Admin.Cohort

  def read_learners_from_file() do
    data = "./lib/ergo/data_upload/data/all_learners.csv" \
    |> File.stream!()\
    |> CSV.decode\
    |> Enum.to_list\
    |> Enum.map(fn
        {:ok, result} -> result
        :error -> IO.puts "Error"
        end)

    # If the line below fails - the headers are set incorrectly.
    [["Cohort", " LearnerName", " LearnerEmail", " LearnerActive"] | learners] = data
    learners
  end

  def unique_cohorts(learners_data) do
    learners_data
    |> Enum.map(&(List.first(&1)))
    |> Enum.uniq()
  end

  def merge_maps([head|rest], acc) do
    updated_acc = Map.merge(acc, head)
    merge_maps(rest, updated_acc)
  end

  def merge_maps([], acc), do: acc

  def seed_cohorts(unique_cohorts) do
    {:ok, cohorts} = Repo.transaction(fn ->
      unique_cohorts
      |> Enum.map(&(Repo.insert!(%Cohort{name: &1})))
    end)
    cohorts
    |> Enum.map(&(Map.from_struct(&1)))
    |> Enum.map(&(%{Map.get(&1, :name) => Map.get(&1, :id) }))
    |> merge_maps(%{})
  end
end

# Ergo.Seeds.read_learners_from_file()

