defmodule Ergo.Mentorship do
  @moduledoc """
  The Mentorship context.
  """

  import Ecto.Query, warn: false
  alias Ergo.Repo

  alias Ergo.Mentorship.ProjectSection

  @doc """
  Returns the list of project_sections.

  ## Examples

      iex> list_project_sections()
      [%ProjectSection{}, ...]

  """
  def list_project_sections do
    Repo.all(ProjectSection)
  end

  def list_project_sections_for_module_id(module_id) do
    query = from p in ProjectSection, where: p.module_id == ^module_id
    Repo.all(query)
  end

  @doc """
  Gets a single project_section.

  Raises `Ecto.NoResultsError` if the Project section does not exist.

  ## Examples

      iex> get_project_section!(123)
      %ProjectSection{}

      iex> get_project_section!(456)
      ** (Ecto.NoResultsError)

  """
  def get_project_section!(id), do: Repo.get!(ProjectSection, id)

  @doc """
  Creates a project_section.

  ## Examples

      iex> create_project_section(%{field: value})
      {:ok, %ProjectSection{}}

      iex> create_project_section(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_project_section(attrs \\ %{}) do
    %ProjectSection{}
    |> ProjectSection.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a project_section.

  ## Examples

      iex> update_project_section(project_section, %{field: new_value})
      {:ok, %ProjectSection{}}

      iex> update_project_section(project_section, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_project_section(%ProjectSection{} = project_section, attrs) do
    project_section
    |> ProjectSection.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ProjectSection.

  ## Examples

      iex> delete_project_section(project_section)
      {:ok, %ProjectSection{}}

      iex> delete_project_section(project_section)
      {:error, %Ecto.Changeset{}}

  """
  def delete_project_section(%ProjectSection{} = project_section) do
    Repo.delete(project_section)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking project_section changes.

  ## Examples

      iex> change_project_section(project_section)
      %Ecto.Changeset{source: %ProjectSection{}}

  """
  def change_project_section(%ProjectSection{} = project_section) do
    ProjectSection.changeset(project_section, %{})
  end

  alias Ergo.Mentorship.MarkedProject

  @doc """
  Returns the list of marked_projects.

  ## Examples

      iex> list_marked_projects()
      [%MarkedProject{}, ...]

  """
  def list_marked_projects do
    Repo.all(MarkedProject)
  end

  @doc """
  Gets a single marked_project.

  Raises `Ecto.NoResultsError` if the Marked project does not exist.

  ## Examples

      iex> get_marked_project!(123)
      %MarkedProject{}

      iex> get_marked_project!(456)
      ** (Ecto.NoResultsError)

  """
  def get_marked_project!(id), do: Repo.get!(MarkedProject, id)

  @doc """
  Creates a marked_project.

  ## Examples

      iex> create_marked_project(%{field: value})
      {:ok, %MarkedProject{}}

      iex> create_marked_project(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_marked_project(attrs \\ %{}) do
    %MarkedProject{}
    |> MarkedProject.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a marked_project.

  ## Examples

      iex> update_marked_project(marked_project, %{field: new_value})
      {:ok, %MarkedProject{}}

      iex> update_marked_project(marked_project, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_marked_project(%MarkedProject{} = marked_project, attrs) do
    marked_project
    |> MarkedProject.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a MarkedProject.

  ## Examples

      iex> delete_marked_project(marked_project)
      {:ok, %MarkedProject{}}

      iex> delete_marked_project(marked_project)
      {:error, %Ecto.Changeset{}}

  """
  def delete_marked_project(%MarkedProject{} = marked_project) do
    Repo.delete(marked_project)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking marked_project changes.

  ## Examples

      iex> change_marked_project(marked_project)
      %Ecto.Changeset{source: %MarkedProject{}}

  """
  def change_marked_project(%MarkedProject{} = marked_project) do
    MarkedProject.changeset(marked_project, %{})
  end

  alias Ergo.Mentorship.Module

  @doc """
  Returns the list of modules.

  ## Examples

      iex> list_modules()
      [%Module{}, ...]

  """
  def list_modules do
    Repo.all(Module)
  end

  @doc """
  Gets a single module.

  Raises `Ecto.NoResultsError` if the Module does not exist.

  ## Examples

      iex> get_module!(123)
      %Module{}

      iex> get_module!(456)
      ** (Ecto.NoResultsError)

  """
  def get_module!(id), do: Repo.get!(Module, id)

  @doc """
  Creates a module.

  ## Examples

      iex> create_module(%{field: value})
      {:ok, %Module{}}

      iex> create_module(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_module(attrs \\ %{}) do
    %Module{}
    |> Module.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a module.

  ## Examples

      iex> update_module(module, %{field: new_value})
      {:ok, %Module{}}

      iex> update_module(module, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_module(%Module{} = module, attrs) do
    module
    |> Module.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Module.

  ## Examples

      iex> delete_module(module)
      {:ok, %Module{}}

      iex> delete_module(module)
      {:error, %Ecto.Changeset{}}

  """
  def delete_module(%Module{} = module) do
    Repo.delete(module)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking module changes.

  ## Examples

      iex> change_module(module)
      %Ecto.Changeset{source: %Module{}}

  """
  def change_module(%Module{} = module) do
    Module.changeset(module, %{})
  end

  alias Ergo.Mentorship.Feedback

  @doc """
  Returns the list of feedback.

  ## Examples

      iex> list_feedback()
      [%Feedback{}, ...]

  """
  def list_feedback do
    Repo.all(Feedback)
  end

  def list_feedback_for_module_id(module_id) do
    feedback_query = from(f in Feedback)
    project_sections_query = from p in ProjectSection, where: p.module_id == ^module_id

    query =
      from f in feedback_query,
        join: p in subquery(project_sections_query),
        on: p.id == f.project_section_id,
        select: %{
          id: f.id,
          name: f.feedback,
          section: p.name,
          category: "Custom"
        }

    Repo.all(query)
  end

  @doc """
  Gets a single feedback.

  Raises `Ecto.NoResultsError` if the Feedback does not exist.

  ## Examples

      iex> get_feedback!(123)
      %Feedback{}

      iex> get_feedback!(456)
      ** (Ecto.NoResultsError)

  """
  def get_feedback!(id), do: Repo.get!(Feedback, id)

  @doc """
  Creates a feedback.

  ## Examples

      iex> create_feedback(%{field: value})
      {:ok, %Feedback{}}

      iex> create_feedback(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_feedback(attrs \\ %{}) do
    %Feedback{}
    |> Feedback.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a feedback.

  ## Examples

      iex> update_feedback(feedback, %{field: new_value})
      {:ok, %Feedback{}}

      iex> update_feedback(feedback, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_feedback(%Feedback{} = feedback, attrs) do
    feedback
    |> Feedback.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Feedback.

  ## Examples

      iex> delete_feedback(feedback)
      {:ok, %Feedback{}}

      iex> delete_feedback(feedback)
      {:error, %Ecto.Changeset{}}

  """
  def delete_feedback(%Feedback{} = feedback) do
    Repo.delete(feedback)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking feedback changes.

  ## Examples

      iex> change_feedback(feedback)
      %Ecto.Changeset{source: %Feedback{}}

  """
  def change_feedback(%Feedback{} = feedback) do
    Feedback.changeset(feedback, %{})
  end
end
