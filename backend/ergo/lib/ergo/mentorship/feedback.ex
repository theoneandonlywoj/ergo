defmodule Ergo.Mentorship.Feedback do
  use Ecto.Schema
  import Ecto.Changeset

  schema "feedback" do
    field :feedback, :string

    belongs_to :project_section, Ergo.Mentorship.ProjectSection

    timestamps()
  end

  @doc false
  def changeset(feedback, attrs) do
    feedback
    |> cast(attrs, [:feedback, :project_section_id])
    |> validate_required([:feedback, :project_section_id])
    |> assoc_constraint(:project_section)
  end
end
