defmodule Ergo.Mentorship.MarkedProject do
  use Ecto.Schema
  import Ecto.Changeset

  schema "marked_projects" do
    field :comments, :string
    field :grade, :string
    field :marker, :string

    # field :module, :string
    # field :learner, :string

    belongs_to :learner, Ergo.Admin.Learner
    belongs_to :module, Ergo.Mentorship.Module

    timestamps()
  end

  @doc false
  def changeset(marked_project, attrs) do
    marked_project
    |> cast(attrs, [:learner_id, :marker, :module_id, :grade, :comments])
    |> validate_required([:learner_id, :marker, :module_id, :grade, :comments])
    |> assoc_constraint(:learner)
    |> assoc_constraint(:module)
  end
end
