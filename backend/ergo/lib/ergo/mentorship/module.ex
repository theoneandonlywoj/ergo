defmodule Ergo.Mentorship.Module do
  use Ecto.Schema
  import Ecto.Changeset

  schema "modules" do
    field :name, :string

    has_many :project_sections, Ergo.Mentorship.ProjectSection
    has_many :marked_project, Ergo.Mentorship.MarkedProject

    timestamps()
  end

  @doc false
  def changeset(module, attrs) do
    module
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
