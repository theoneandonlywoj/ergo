defmodule Ergo.Mentorship.ProjectSection do
  use Ecto.Schema
  import Ecto.Changeset

  schema "project_sections" do
    field :name, :string

    belongs_to :module, Ergo.Mentorship.Module
    has_many :feedback, Ergo.Mentorship.Feedback

    timestamps()
  end

  @doc false
  def changeset(project_section, attrs) do
    project_section
    |> cast(attrs, [:name, :module_id])
    |> validate_required([:name, :module_id])
    |> assoc_constraint(:module)
  end
end
