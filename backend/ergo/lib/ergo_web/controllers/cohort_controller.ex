defmodule ErgoWeb.CohortController do
  use ErgoWeb, :controller

  alias Ergo.Admin
  alias Ergo.Admin.Cohort

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    cohorts = Admin.list_cohorts()
    render(conn, "index.json", cohorts: cohorts)
  end

  def index_user_subscription(conn, %{"user_id" => user_id}) do
    cohorts = Admin.list_cohorts_for_user_subscriptions(user_id)
    render(conn, "index.json", cohorts: cohorts)
  end

  def create(conn, %{"cohort" => cohort_params}) do
    with {:ok, %Cohort{} = cohort} <- Admin.create_cohort(cohort_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.cohort_path(conn, :show, cohort))
      |> render("show.json", cohort: cohort)
    end
  end

  def show(conn, %{"id" => id}) do
    cohort = Admin.get_cohort!(id)
    render(conn, "show.json", cohort: cohort)
  end

  def update(conn, %{"id" => id, "cohort" => cohort_params}) do
    cohort = Admin.get_cohort!(id)

    with {:ok, %Cohort{} = cohort} <- Admin.update_cohort(cohort, cohort_params) do
      render(conn, "show.json", cohort: cohort)
    end
  end

  def delete(conn, %{"id" => id}) do
    cohort = Admin.get_cohort!(id)

    with {:ok, %Cohort{}} <- Admin.delete_cohort(cohort) do
      send_resp(conn, :no_content, "")
    end
  end
end
