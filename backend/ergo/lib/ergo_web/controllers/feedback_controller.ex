defmodule ErgoWeb.FeedbackController do
  use ErgoWeb, :controller

  alias Ergo.Mentorship
  alias Ergo.Mentorship.Feedback

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    feedback = Mentorship.list_feedback()
    render(conn, "index.json", feedback: feedback)
  end

  def index_for_module_id(conn, %{"module_id" => module_id}) do
    feedback = Mentorship.list_feedback_for_module_id(module_id)
    render(conn, "index_for_module_id.json", feedback: feedback)
  end

  def create(conn, %{"feedback" => feedback_params}) do
    with {:ok, %Feedback{} = feedback} <- Mentorship.create_feedback(feedback_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.feedback_path(conn, :show, feedback))
      |> render("show.json", feedback: feedback)
    end
  end

  def show(conn, %{"id" => id}) do
    feedback = Mentorship.get_feedback!(id)
    render(conn, "show.json", feedback: feedback)
  end

  def update(conn, %{"id" => id, "feedback" => feedback_params}) do
    feedback = Mentorship.get_feedback!(id)

    with {:ok, %Feedback{} = feedback} <- Mentorship.update_feedback(feedback, feedback_params) do
      render(conn, "show.json", feedback: feedback)
    end
  end

  def delete(conn, %{"id" => id}) do
    feedback = Mentorship.get_feedback!(id)

    with {:ok, %Feedback{}} <- Mentorship.delete_feedback(feedback) do
      send_resp(conn, :no_content, "")
    end
  end
end
