defmodule ErgoWeb.LearnerController do
  use ErgoWeb, :controller

  alias Ergo.Admin
  alias Ergo.Admin.Learner

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    learners = Admin.list_learners()
    render(conn, "index.json", learners: learners)
  end

  def index_for_cohort_id(conn, %{"cohort_id" => cohort_id}) do
    learners = Admin.list_learners_for_cohort_id(cohort_id)
    render(conn, "index.json", learners: learners)
  end

  def create(conn, %{"learner" => learner_params}) do
    with {:ok, %Learner{} = learner} <- Admin.create_learner(learner_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.learner_path(conn, :show, learner))
      |> render("show.json", learner: learner)
    end
  end

  def show(conn, %{"id" => id}) do
    learner = Admin.get_learner!(id)
    render(conn, "show.json", learner: learner)
  end

  def update(conn, %{"id" => id, "learner" => learner_params}) do
    learner = Admin.get_learner!(id)

    with {:ok, %Learner{} = learner} <- Admin.update_learner(learner, learner_params) do
      render(conn, "show.json", learner: learner)
    end
  end

  def delete(conn, %{"id" => id}) do
    learner = Admin.get_learner!(id)

    with {:ok, %Learner{}} <- Admin.delete_learner(learner) do
      send_resp(conn, :no_content, "")
    end
  end
end
