defmodule ErgoWeb.LearnerEventController do
  use ErgoWeb, :controller

  alias Ergo.Admin
  alias Ergo.Admin.LearnerEvent

  action_fallback ErgoWeb.FallbackController

  def index(conn, params) do
    learner_events = Admin.list_learner_events(params)
    render(conn, "index.json", learner_events: learner_events)
  end

  def create(conn, %{"learner_event" => learner_event_params}) do
    with {:ok, %LearnerEvent{} = learner_event} <-
           Admin.create_learner_event(learner_event_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.learner_event_path(conn, :show, learner_event))
      |> render("show.json", learner_event: learner_event)
    end
  end

  def show(conn, %{"id" => id}) do
    learner_event = Admin.get_learner_event!(id)
    render(conn, "show.json", learner_event: learner_event)
  end

  def update(conn, %{"id" => id, "learner_event" => learner_event_params}) do
    learner_event = Admin.get_learner_event!(id)

    with {:ok, %LearnerEvent{} = learner_event} <-
           Admin.update_learner_event(learner_event, learner_event_params) do
      render(conn, "show.json", learner_event: learner_event)
    end
  end

  def delete(conn, %{"id" => id}) do
    learner_event = Admin.get_learner_event!(id)

    with {:ok, %LearnerEvent{}} <- Admin.delete_learner_event(learner_event) do
      send_resp(conn, :no_content, "")
    end
  end
end
