defmodule ErgoWeb.MarkedProjectController do
  use ErgoWeb, :controller

  alias Ergo.Mentorship
  alias Ergo.Mentorship.MarkedProject

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    marked_projects = Mentorship.list_marked_projects()
    render(conn, "index.json", marked_projects: marked_projects)
  end

  def create(conn, %{"marked_project" => marked_project_params}) do
    with {:ok, %MarkedProject{} = marked_project} <-
           Mentorship.create_marked_project(marked_project_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.marked_project_path(conn, :show, marked_project))
      |> render("show.json", marked_project: marked_project)
    end
  end

  def show(conn, %{"id" => id}) do
    marked_project = Mentorship.get_marked_project!(id)
    render(conn, "show.json", marked_project: marked_project)
  end

  def update(conn, %{"id" => id, "marked_project" => marked_project_params}) do
    marked_project = Mentorship.get_marked_project!(id)

    with {:ok, %MarkedProject{} = marked_project} <-
           Mentorship.update_marked_project(marked_project, marked_project_params) do
      render(conn, "show.json", marked_project: marked_project)
    end
  end

  def delete(conn, %{"id" => id}) do
    marked_project = Mentorship.get_marked_project!(id)

    with {:ok, %MarkedProject{}} <- Mentorship.delete_marked_project(marked_project) do
      send_resp(conn, :no_content, "")
    end
  end
end
