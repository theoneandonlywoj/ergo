defmodule ErgoWeb.ModuleController do
  use ErgoWeb, :controller

  alias Ergo.Mentorship
  alias Ergo.Mentorship.Module

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    modules = Mentorship.list_modules()
    render(conn, "index.json", modules: modules)
  end

  def create(conn, %{"module" => module_params}) do
    with {:ok, %Module{} = module} <- Mentorship.create_module(module_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.module_path(conn, :show, module))
      |> render("show.json", module: module)
    end
  end

  def show(conn, %{"id" => id}) do
    module = Mentorship.get_module!(id)
    render(conn, "show.json", module: module)
  end

  def update(conn, %{"id" => id, "module" => module_params}) do
    module = Mentorship.get_module!(id)

    with {:ok, %Module{} = module} <- Mentorship.update_module(module, module_params) do
      render(conn, "show.json", module: module)
    end
  end

  def delete(conn, %{"id" => id}) do
    module = Mentorship.get_module!(id)

    with {:ok, %Module{}} <- Mentorship.delete_module(module) do
      send_resp(conn, :no_content, "")
    end
  end
end
