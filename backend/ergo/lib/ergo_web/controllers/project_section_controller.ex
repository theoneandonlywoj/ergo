defmodule ErgoWeb.ProjectSectionController do
  use ErgoWeb, :controller

  alias Ergo.Mentorship
  alias Ergo.Mentorship.ProjectSection

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    project_sections = Mentorship.list_project_sections()
    render(conn, "index.json", project_sections: project_sections)
  end

  def index_for_module_id(conn, %{"module_id" => module_id}) do
    project_sections = Mentorship.list_project_sections_for_module_id(module_id)
    render(conn, "index.json", project_sections: project_sections)
  end

  def create(conn, %{"project_section" => project_section_params}) do
    with {:ok, %ProjectSection{} = project_section} <-
           Mentorship.create_project_section(project_section_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.project_section_path(conn, :show, project_section))
      |> render("show.json", project_section: project_section)
    end
  end

  def show(conn, %{"id" => id}) do
    project_section = Mentorship.get_project_section!(id)
    render(conn, "show.json", project_section: project_section)
  end

  def update(conn, %{"id" => id, "project_section" => project_section_params}) do
    project_section = Mentorship.get_project_section!(id)

    with {:ok, %ProjectSection{} = project_section} <-
           Mentorship.update_project_section(project_section, project_section_params) do
      render(conn, "show.json", project_section: project_section)
    end
  end

  def delete(conn, %{"id" => id}) do
    project_section = Mentorship.get_project_section!(id)

    with {:ok, %ProjectSection{}} <- Mentorship.delete_project_section(project_section) do
      send_resp(conn, :no_content, "")
    end
  end
end
