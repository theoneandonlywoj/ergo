defmodule ErgoWeb.UserCohortSubscriptionController do
  use ErgoWeb, :controller

  alias Ergo.Admin
  alias Ergo.Admin.UserCohortSubscription

  action_fallback ErgoWeb.FallbackController

  def index(conn, _params) do
    user_cohort_subscriptions = Admin.list_user_cohort_subscriptions()
    render(conn, "index.json", user_cohort_subscriptions: user_cohort_subscriptions)
  end

  def create(conn, %{"user_cohort_subscription" => user_cohort_subscription_params}) do
    with {:ok, %UserCohortSubscription{} = user_cohort_subscription} <-
           Admin.create_user_cohort_subscription(user_cohort_subscription_params) do
      conn
      |> put_status(:created)
      |> put_resp_header(
        "location",
        Routes.user_cohort_subscription_path(conn, :show, user_cohort_subscription)
      )
      |> render("show.json", user_cohort_subscription: user_cohort_subscription)
    end
  end

  def show(conn, %{"id" => id}) do
    user_cohort_subscription = Admin.get_user_cohort_subscription!(id)
    render(conn, "show.json", user_cohort_subscription: user_cohort_subscription)
  end

  def update(conn, %{"id" => id, "user_cohort_subscription" => user_cohort_subscription_params}) do
    user_cohort_subscription = Admin.get_user_cohort_subscription!(id)

    with {:ok, %UserCohortSubscription{} = user_cohort_subscription} <-
           Admin.update_user_cohort_subscription(
             user_cohort_subscription,
             user_cohort_subscription_params
           ) do
      render(conn, "show.json", user_cohort_subscription: user_cohort_subscription)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_cohort_subscription = Admin.get_user_cohort_subscription!(id)

    with {:ok, %UserCohortSubscription{}} <-
           Admin.delete_user_cohort_subscription(user_cohort_subscription) do
      send_resp(conn, :no_content, "")
    end
  end
end
