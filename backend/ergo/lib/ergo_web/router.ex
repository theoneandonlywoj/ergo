defmodule ErgoWeb.Router do
  use ErgoWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ErgoWeb do
    pipe_through :api
    # Admin
    resources "/users", UserController, except: [:delete]
    post "/sign_in", UserController, :sign_in
    resources "/cohorts", CohortController, except: [:delete]
    get "/cohorts_users_subscribed_to", CohortController, :index_user_subscription
    resources "/learners", LearnerController, except: [:delete]
    get "/learners_for_cohort_id", LearnerController, :index_for_cohort_id
    resources "/user_cohort_subscriptions", UserCohortSubscriptionController
    resources "/learner_events", LearnerEventController
    # Mentorship
    resources "/feedback", FeedbackController, except: [:delete]
    get "/feedback_for_module_id", FeedbackController, :index_for_module_id
    resources "/project_sections", ProjectSectionController, except: [:delete]
    get "/project_sections_for_module_id", ProjectSectionController, :index_for_module_id
    resources "/marked_projects", MarkedProjectController, except: [:delete]
    resources "/modules", ModuleController, except: [:delete]
  end
end
