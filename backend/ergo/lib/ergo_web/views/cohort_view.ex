defmodule ErgoWeb.CohortView do
  use ErgoWeb, :view
  alias ErgoWeb.CohortView

  def render("index.json", %{cohorts: cohorts}) do
    %{data: render_many(cohorts, CohortView, "cohort.json")}
  end

  def render("show.json", %{cohort: cohort}) do
    %{data: render_one(cohort, CohortView, "cohort.json")}
  end

  def render("cohort.json", %{cohort: cohort}) do
    %{id: cohort.id, name: cohort.name}
  end
end
