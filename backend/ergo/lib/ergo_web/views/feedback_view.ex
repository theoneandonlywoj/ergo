defmodule ErgoWeb.FeedbackView do
  use ErgoWeb, :view
  alias ErgoWeb.FeedbackView

  def render("index.json", %{feedback: feedback}) do
    %{data: render_many(feedback, FeedbackView, "feedback.json")}
  end

  def render("show.json", %{feedback: feedback}) do
    %{data: render_one(feedback, FeedbackView, "feedback.json")}
  end

  def render("feedback.json", %{feedback: feedback}) do
    %{
      id: feedback.id,
      feedback: feedback.feedback,
      project_section_id: feedback.project_section_id
    }
  end

  def render("index_for_module_id.json", %{feedback: feedback}) do
    %{data: render_many(feedback, FeedbackView, "feedback_for_module_id.json")}
  end

  def render("feedback_for_module_id.json", %{feedback: feedback}) do
    %{
      id: feedback.id,
      name: feedback.name,
      category: feedback.category,
      section: feedback.section
    }
  end
end
