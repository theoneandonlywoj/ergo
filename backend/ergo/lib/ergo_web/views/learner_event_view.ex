defmodule ErgoWeb.LearnerEventView do
  use ErgoWeb, :view
  alias ErgoWeb.LearnerEventView

  def render("index.json", %{learner_events: learner_events}) do
    %{data: render_many(learner_events, LearnerEventView, "learner_event.json")}
  end

  def render("show.json", %{learner_event: learner_event}) do
    %{data: render_one(learner_event, LearnerEventView, "learner_event.json")}
  end

  def render("learner_event.json", %{learner_event: learner_event}) do
    %{
      id: learner_event.id,
      learner_id: learner_event.learner_id,
      type: learner_event.type,
      details: learner_event.details
    }
  end
end
