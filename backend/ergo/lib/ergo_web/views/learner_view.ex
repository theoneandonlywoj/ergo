defmodule ErgoWeb.LearnerView do
  use ErgoWeb, :view
  alias ErgoWeb.LearnerView

  def render("index.json", %{learners: learners}) do
    %{data: render_many(learners, LearnerView, "learner.json")}
  end

  def render("show.json", %{learner: learner}) do
    %{data: render_one(learner, LearnerView, "learner.json")}
  end

  def render("learner.json", %{learner: learner}) do
    %{
      id: learner.id,
      name: learner.name,
      rag_status: learner.rag_status,
      cohort_id: learner.cohort_id
    }
  end
end
