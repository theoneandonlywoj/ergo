defmodule ErgoWeb.MarkedProjectView do
  use ErgoWeb, :view
  alias ErgoWeb.MarkedProjectView

  def render("index.json", %{marked_projects: marked_projects}) do
    %{data: render_many(marked_projects, MarkedProjectView, "marked_project.json")}
  end

  def render("show.json", %{marked_project: marked_project}) do
    %{data: render_one(marked_project, MarkedProjectView, "marked_project.json")}
  end

  def render("marked_project.json", %{marked_project: marked_project}) do
    %{
      id: marked_project.id,
      learner_id: marked_project.learner_id,
      marker: marked_project.marker,
      module_id: marked_project.module_id,
      grade: marked_project.grade,
      comments: marked_project.comments
    }
  end
end
