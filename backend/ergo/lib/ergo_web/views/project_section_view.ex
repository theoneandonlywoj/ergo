defmodule ErgoWeb.ProjectSectionView do
  use ErgoWeb, :view
  alias ErgoWeb.ProjectSectionView

  def render("index.json", %{project_sections: project_sections}) do
    %{data: render_many(project_sections, ProjectSectionView, "project_section.json")}
  end

  def render("show.json", %{project_section: project_section}) do
    %{data: render_one(project_section, ProjectSectionView, "project_section.json")}
  end

  def render("project_section.json", %{project_section: project_section}) do
    %{id: project_section.id, name: project_section.name, module_id: project_section.module_id}
  end
end
