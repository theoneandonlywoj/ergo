defmodule ErgoWeb.UserCohortSubscriptionView do
  use ErgoWeb, :view
  alias ErgoWeb.UserCohortSubscriptionView

  def render("index.json", %{user_cohort_subscriptions: user_cohort_subscriptions}) do
    %{
      data:
        render_many(
          user_cohort_subscriptions,
          UserCohortSubscriptionView,
          "user_cohort_subscription.json"
        )
    }
  end

  def render("show.json", %{user_cohort_subscription: user_cohort_subscription}) do
    %{
      data:
        render_one(
          user_cohort_subscription,
          UserCohortSubscriptionView,
          "user_cohort_subscription.json"
        )
    }
  end

  def render("user_cohort_subscription.json", %{
        user_cohort_subscription: user_cohort_subscription
      }) do
    %{
      id: user_cohort_subscription.id,
      user_id: user_cohort_subscription.user_id,
      cohort_id: user_cohort_subscription.cohort_id
    }
  end
end
