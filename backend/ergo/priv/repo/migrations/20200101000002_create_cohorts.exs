defmodule Ergo.Repo.Migrations.CreateCohorts do
  use Ecto.Migration

  def change do
    create table(:cohorts) do
      add :name, :string

      timestamps()
    end
  end
end
