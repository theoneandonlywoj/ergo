defmodule Ergo.Repo.Migrations.CreateUserCohortSubscriptions do
  use Ecto.Migration

  def change do
    create table(:user_cohort_subscriptions) do
      add :user_id, references(:users, on_delete: :nothing), null: false
      add :cohort_id, references(:cohorts, on_delete: :nothing), null: false

      timestamps()
    end
  end
end
