defmodule Ergo.Repo.Migrations.CreateLearners do
  use Ecto.Migration

  def change do
    create table(:learners) do
      add :name, :string
      add :rag_status, :string, default: "Green"
      add :cohort_id, references(:cohorts, on_delete: :nothing), null: false

      timestamps()
    end
  end
end
