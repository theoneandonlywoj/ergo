defmodule Ergo.Repo.Migrations.CreateLearnerEvents do
  use Ecto.Migration

  def change do
    create table(:learner_events) do
      add :type, :string
      add :details, :text

      add :learner_id, references(:learners, on_delete: :nothing), null: false

      timestamps()
    end
  end
end
