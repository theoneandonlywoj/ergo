defmodule Ergo.Repo.Migrations.CreateProjectSections do
  use Ecto.Migration

  def change do
    create table(:project_sections) do
      add :name, :string

      add :module_id, references(:modules, on_delete: :nothing), null: false

      timestamps()
    end
  end
end
