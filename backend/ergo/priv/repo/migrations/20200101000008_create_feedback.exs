defmodule Ergo.Repo.Migrations.CreateFeedback do
  use Ecto.Migration

  def change do
    create table(:feedback) do
      add :feedback, :text

      add :project_section_id, references(:project_sections, on_delete: :nothing), null: false

      timestamps()
    end
  end
end
