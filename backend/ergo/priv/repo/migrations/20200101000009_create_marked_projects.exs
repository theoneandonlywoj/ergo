defmodule Ergo.Repo.Migrations.CreateMarkedProjects do
  use Ecto.Migration

  def change do
    create table(:marked_projects) do
      # add :learner, :string
      add :learner_id,
          references(:learners, on_delete: :nothing),
          null: false

      add :marker, :string
      # add :module, :string
      add :module_id,
          references(:modules, on_delete: :nothing),
          null: false

      add :grade, :string
      add :comments, :text

      timestamps()
    end
  end
end
