# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Ergo.Repo.insert!(%Ergo.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Ergo.Repo
alias Ergo.Admin.User
alias Ergo.Admin.Cohort
alias Ergo.Admin.Learner
alias Ergo.Admin.UserCohortSubscription
alias Ergo.Mentorship.ProjectSection
alias Ergo.Mentorship.Module

# Users
Repo.insert!(%User{email: "wojciech@decoded.com", name: "Wojciech Orzechowski", password: "Felix0000"})

# Cohorts
Repo.insert!(%Cohort{name: "Decoded Test"})
Repo.insert!(%Cohort{name: "RS Alpha"})

# Cohort Subscription
Repo.insert(%UserCohortSubscription{user_id: 1, cohort_id: 1})
Repo.insert(%UserCohortSubscription{user_id: 1, cohort_id: 2})

# Learners (sample)
for i <- 1..40 do
  Repo.insert!(%Learner{name: "Learner " <> to_string(i), cohort_id: 1})
end

for i <- 1..40 do
  Repo.insert!(%Learner{name: "RS Learner " <> to_string(i), cohort_id: 2})
end

# Modules
Repo.insert!(%Module{name: "SQL"})
non_sql_modules = ["Clustering", "Association Rules", "Regression", "Clustering"]

for module <- non_sql_modules do
  Repo.insert!(%Module{name: module})
end

# Project Sections for SQL as it was added first and will have id = 1
Repo.insert!(%ProjectSection{name: "Business Case Presentation", module_id: 1})
Repo.insert!(%ProjectSection{name: "Modelling", module_id: 1})
Repo.insert!(%ProjectSection{name: "Implementation", module_id: 1})
Repo.insert!(%ProjectSection{name: "Analysis", module_id: 1})
Repo.insert!(%ProjectSection{name: "Presentation", module_id: 1})

Repo.insert!(%ProjectSection{name: "Business Case Presentation", module_id: 2})
Repo.insert!(%ProjectSection{name: "Modelling", module_id: 2})
Repo.insert!(%ProjectSection{name: "Implementation", module_id: 2})
