defmodule Ergo.AdminTest do
  use Ergo.DataCase

  alias Ergo.Admin

  describe "cohorts" do
    alias Ergo.Admin.Cohort

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def cohort_fixture(attrs \\ %{}) do
      {:ok, cohort} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Admin.create_cohort()

      cohort
    end

    test "list_cohorts/0 returns all cohorts" do
      cohort = cohort_fixture()
      assert Admin.list_cohorts() == [cohort]
    end

    test "get_cohort!/1 returns the cohort with given id" do
      cohort = cohort_fixture()
      assert Admin.get_cohort!(cohort.id) == cohort
    end

    test "create_cohort/1 with valid data creates a cohort" do
      assert {:ok, %Cohort{} = cohort} = Admin.create_cohort(@valid_attrs)
      assert cohort.name == "some name"
    end

    test "create_cohort/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_cohort(@invalid_attrs)
    end

    test "update_cohort/2 with valid data updates the cohort" do
      cohort = cohort_fixture()
      assert {:ok, %Cohort{} = cohort} = Admin.update_cohort(cohort, @update_attrs)
      assert cohort.name == "some updated name"
    end

    test "update_cohort/2 with invalid data returns error changeset" do
      cohort = cohort_fixture()
      assert {:error, %Ecto.Changeset{}} = Admin.update_cohort(cohort, @invalid_attrs)
      assert cohort == Admin.get_cohort!(cohort.id)
    end

    test "delete_cohort/1 deletes the cohort" do
      cohort = cohort_fixture()
      assert {:ok, %Cohort{}} = Admin.delete_cohort(cohort)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_cohort!(cohort.id) end
    end

    test "change_cohort/1 returns a cohort changeset" do
      cohort = cohort_fixture()
      assert %Ecto.Changeset{} = Admin.change_cohort(cohort)
    end
  end

  describe "learners" do
    alias Ergo.Admin.Learner

    @valid_attrs %{cohort_id: 42, name: "some name"}
    @update_attrs %{cohort_id: 43, name: "some updated name"}
    @invalid_attrs %{cohort_id: nil, name: nil}

    def learner_fixture(attrs \\ %{}) do
      {:ok, learner} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Admin.create_learner()

      learner
    end

    test "list_learners/0 returns all learners" do
      learner = learner_fixture()
      assert Admin.list_learners() == [learner]
    end

    test "get_learner!/1 returns the learner with given id" do
      learner = learner_fixture()
      assert Admin.get_learner!(learner.id) == learner
    end

    test "create_learner/1 with valid data creates a learner" do
      assert {:ok, %Learner{} = learner} = Admin.create_learner(@valid_attrs)
      assert learner.cohort_id == 42
      assert learner.name == "some name"
    end

    test "create_learner/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_learner(@invalid_attrs)
    end

    test "update_learner/2 with valid data updates the learner" do
      learner = learner_fixture()
      assert {:ok, %Learner{} = learner} = Admin.update_learner(learner, @update_attrs)
      assert learner.cohort_id == 43
      assert learner.name == "some updated name"
    end

    test "update_learner/2 with invalid data returns error changeset" do
      learner = learner_fixture()
      assert {:error, %Ecto.Changeset{}} = Admin.update_learner(learner, @invalid_attrs)
      assert learner == Admin.get_learner!(learner.id)
    end

    test "delete_learner/1 deletes the learner" do
      learner = learner_fixture()
      assert {:ok, %Learner{}} = Admin.delete_learner(learner)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_learner!(learner.id) end
    end

    test "change_learner/1 returns a learner changeset" do
      learner = learner_fixture()
      assert %Ecto.Changeset{} = Admin.change_learner(learner)
    end
  end

  describe "users" do
    alias Ergo.Admin.User

    @valid_attrs %{email: "some email", password: "some password"}
    @update_attrs %{email: "some updated email", password: "some updated password"}
    @invalid_attrs %{email: nil, password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Admin.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Admin.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Admin.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Admin.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.password == "some password"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Admin.update_user(user, @update_attrs)
      assert user.email == "some updated email"
      assert user.password == "some updated password"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Admin.update_user(user, @invalid_attrs)
      assert user == Admin.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Admin.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Admin.change_user(user)
    end
  end

  describe "user_cohort_subscriptions" do
    alias Ergo.Admin.UserCohortSubscription

    @valid_attrs %{cohort_id: 42, user_id: 42}
    @update_attrs %{cohort_id: 43, user_id: 43}
    @invalid_attrs %{cohort_id: nil, user_id: nil}

    def user_cohort_subscription_fixture(attrs \\ %{}) do
      {:ok, user_cohort_subscription} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Admin.create_user_cohort_subscription()

      user_cohort_subscription
    end

    test "list_user_cohort_subscriptions/0 returns all user_cohort_subscriptions" do
      user_cohort_subscription = user_cohort_subscription_fixture()
      assert Admin.list_user_cohort_subscriptions() == [user_cohort_subscription]
    end

    test "get_user_cohort_subscription!/1 returns the user_cohort_subscription with given id" do
      user_cohort_subscription = user_cohort_subscription_fixture()

      assert Admin.get_user_cohort_subscription!(user_cohort_subscription.id) ==
               user_cohort_subscription
    end

    test "create_user_cohort_subscription/1 with valid data creates a user_cohort_subscription" do
      assert {:ok, %UserCohortSubscription{} = user_cohort_subscription} =
               Admin.create_user_cohort_subscription(@valid_attrs)

      assert user_cohort_subscription.cohort_id == 42
      assert user_cohort_subscription.user_id == 42
    end

    test "create_user_cohort_subscription/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_user_cohort_subscription(@invalid_attrs)
    end

    test "update_user_cohort_subscription/2 with valid data updates the user_cohort_subscription" do
      user_cohort_subscription = user_cohort_subscription_fixture()

      assert {:ok, %UserCohortSubscription{} = user_cohort_subscription} =
               Admin.update_user_cohort_subscription(user_cohort_subscription, @update_attrs)

      assert user_cohort_subscription.cohort_id == 43
      assert user_cohort_subscription.user_id == 43
    end

    test "update_user_cohort_subscription/2 with invalid data returns error changeset" do
      user_cohort_subscription = user_cohort_subscription_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Admin.update_user_cohort_subscription(user_cohort_subscription, @invalid_attrs)

      assert user_cohort_subscription ==
               Admin.get_user_cohort_subscription!(user_cohort_subscription.id)
    end

    test "delete_user_cohort_subscription/1 deletes the user_cohort_subscription" do
      user_cohort_subscription = user_cohort_subscription_fixture()

      assert {:ok, %UserCohortSubscription{}} =
               Admin.delete_user_cohort_subscription(user_cohort_subscription)

      assert_raise Ecto.NoResultsError, fn ->
        Admin.get_user_cohort_subscription!(user_cohort_subscription.id)
      end
    end

    test "change_user_cohort_subscription/1 returns a user_cohort_subscription changeset" do
      user_cohort_subscription = user_cohort_subscription_fixture()
      assert %Ecto.Changeset{} = Admin.change_user_cohort_subscription(user_cohort_subscription)
    end
  end

  describe "learner_events" do
    alias Ergo.Admin.LearnerEvent

    @valid_attrs %{details: "some details", learner_id: 42, type: "some type"}
    @update_attrs %{details: "some updated details", learner_id: 43, type: "some updated type"}
    @invalid_attrs %{details: nil, learner_id: nil, type: nil}

    def learner_event_fixture(attrs \\ %{}) do
      {:ok, learner_event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Admin.create_learner_event()

      learner_event
    end

    test "list_learner_events/0 returns all learner_events" do
      learner_event = learner_event_fixture()
      assert Admin.list_learner_events() == [learner_event]
    end

    test "get_learner_event!/1 returns the learner_event with given id" do
      learner_event = learner_event_fixture()
      assert Admin.get_learner_event!(learner_event.id) == learner_event
    end

    test "create_learner_event/1 with valid data creates a learner_event" do
      assert {:ok, %LearnerEvent{} = learner_event} = Admin.create_learner_event(@valid_attrs)
      assert learner_event.details == "some details"
      assert learner_event.learner_id == 42
      assert learner_event.type == "some type"
    end

    test "create_learner_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Admin.create_learner_event(@invalid_attrs)
    end

    test "update_learner_event/2 with valid data updates the learner_event" do
      learner_event = learner_event_fixture()

      assert {:ok, %LearnerEvent{} = learner_event} =
               Admin.update_learner_event(learner_event, @update_attrs)

      assert learner_event.details == "some updated details"
      assert learner_event.learner_id == 43
      assert learner_event.type == "some updated type"
    end

    test "update_learner_event/2 with invalid data returns error changeset" do
      learner_event = learner_event_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Admin.update_learner_event(learner_event, @invalid_attrs)

      assert learner_event == Admin.get_learner_event!(learner_event.id)
    end

    test "delete_learner_event/1 deletes the learner_event" do
      learner_event = learner_event_fixture()
      assert {:ok, %LearnerEvent{}} = Admin.delete_learner_event(learner_event)
      assert_raise Ecto.NoResultsError, fn -> Admin.get_learner_event!(learner_event.id) end
    end

    test "change_learner_event/1 returns a learner_event changeset" do
      learner_event = learner_event_fixture()
      assert %Ecto.Changeset{} = Admin.change_learner_event(learner_event)
    end
  end
end
