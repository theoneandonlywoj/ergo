defmodule Ergo.MentorshipTest do
  use Ergo.DataCase

  alias Ergo.Mentorship

  describe "projects" do
    alias Ergo.Mentorship.Project

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def project_fixture(attrs \\ %{}) do
      {:ok, project} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Mentorship.create_project()

      project
    end

    test "list_projects/0 returns all projects" do
      project = project_fixture()
      assert Mentorship.list_projects() == [project]
    end

    test "get_project!/1 returns the project with given id" do
      project = project_fixture()
      assert Mentorship.get_project!(project.id) == project
    end

    test "create_project/1 with valid data creates a project" do
      assert {:ok, %Project{} = project} = Mentorship.create_project(@valid_attrs)
      assert project.name == "some name"
    end

    test "create_project/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Mentorship.create_project(@invalid_attrs)
    end

    test "update_project/2 with valid data updates the project" do
      project = project_fixture()
      assert {:ok, %Project{} = project} = Mentorship.update_project(project, @update_attrs)
      assert project.name == "some updated name"
    end

    test "update_project/2 with invalid data returns error changeset" do
      project = project_fixture()
      assert {:error, %Ecto.Changeset{}} = Mentorship.update_project(project, @invalid_attrs)
      assert project == Mentorship.get_project!(project.id)
    end

    test "delete_project/1 deletes the project" do
      project = project_fixture()
      assert {:ok, %Project{}} = Mentorship.delete_project(project)
      assert_raise Ecto.NoResultsError, fn -> Mentorship.get_project!(project.id) end
    end

    test "change_project/1 returns a project changeset" do
      project = project_fixture()
      assert %Ecto.Changeset{} = Mentorship.change_project(project)
    end
  end

  describe "project_sections" do
    alias Ergo.Mentorship.ProjectSection

    @valid_attrs %{name: "some name", project_id: 42}
    @update_attrs %{name: "some updated name", project_id: 43}
    @invalid_attrs %{name: nil, project_id: nil}

    def project_section_fixture(attrs \\ %{}) do
      {:ok, project_section} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Mentorship.create_project_section()

      project_section
    end

    test "list_project_sections/0 returns all project_sections" do
      project_section = project_section_fixture()
      assert Mentorship.list_project_sections() == [project_section]
    end

    test "get_project_section!/1 returns the project_section with given id" do
      project_section = project_section_fixture()
      assert Mentorship.get_project_section!(project_section.id) == project_section
    end

    test "create_project_section/1 with valid data creates a project_section" do
      assert {:ok, %ProjectSection{} = project_section} =
               Mentorship.create_project_section(@valid_attrs)

      assert project_section.name == "some name"
      assert project_section.project_id == 42
    end

    test "create_project_section/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Mentorship.create_project_section(@invalid_attrs)
    end

    test "update_project_section/2 with valid data updates the project_section" do
      project_section = project_section_fixture()

      assert {:ok, %ProjectSection{} = project_section} =
               Mentorship.update_project_section(project_section, @update_attrs)

      assert project_section.name == "some updated name"
      assert project_section.project_id == 43
    end

    test "update_project_section/2 with invalid data returns error changeset" do
      project_section = project_section_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Mentorship.update_project_section(project_section, @invalid_attrs)

      assert project_section == Mentorship.get_project_section!(project_section.id)
    end

    test "delete_project_section/1 deletes the project_section" do
      project_section = project_section_fixture()
      assert {:ok, %ProjectSection{}} = Mentorship.delete_project_section(project_section)

      assert_raise Ecto.NoResultsError, fn ->
        Mentorship.get_project_section!(project_section.id)
      end
    end

    test "change_project_section/1 returns a project_section changeset" do
      project_section = project_section_fixture()
      assert %Ecto.Changeset{} = Mentorship.change_project_section(project_section)
    end
  end

  describe "marked_projects" do
    alias Ergo.Mentorship.MarkedProject

    @valid_attrs %{
      comments: "some comments",
      grade: "some grade",
      learner: "some learner",
      marker: "some marker",
      module: "some module"
    }
    @update_attrs %{
      comments: "some updated comments",
      grade: "some updated grade",
      learner: "some updated learner",
      marker: "some updated marker",
      module: "some updated module"
    }
    @invalid_attrs %{comments: nil, grade: nil, learner: nil, marker: nil, module: nil}

    def marked_project_fixture(attrs \\ %{}) do
      {:ok, marked_project} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Mentorship.create_marked_project()

      marked_project
    end

    test "list_marked_projects/0 returns all marked_projects" do
      marked_project = marked_project_fixture()
      assert Mentorship.list_marked_projects() == [marked_project]
    end

    test "get_marked_project!/1 returns the marked_project with given id" do
      marked_project = marked_project_fixture()
      assert Mentorship.get_marked_project!(marked_project.id) == marked_project
    end

    test "create_marked_project/1 with valid data creates a marked_project" do
      assert {:ok, %MarkedProject{} = marked_project} =
               Mentorship.create_marked_project(@valid_attrs)

      assert marked_project.comments == "some comments"
      assert marked_project.grade == "some grade"
      assert marked_project.learner == "some learner"
      assert marked_project.marker == "some marker"
      assert marked_project.module == "some module"
    end

    test "create_marked_project/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Mentorship.create_marked_project(@invalid_attrs)
    end

    test "update_marked_project/2 with valid data updates the marked_project" do
      marked_project = marked_project_fixture()

      assert {:ok, %MarkedProject{} = marked_project} =
               Mentorship.update_marked_project(marked_project, @update_attrs)

      assert marked_project.comments == "some updated comments"
      assert marked_project.grade == "some updated grade"
      assert marked_project.learner == "some updated learner"
      assert marked_project.marker == "some updated marker"
      assert marked_project.module == "some updated module"
    end

    test "update_marked_project/2 with invalid data returns error changeset" do
      marked_project = marked_project_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Mentorship.update_marked_project(marked_project, @invalid_attrs)

      assert marked_project == Mentorship.get_marked_project!(marked_project.id)
    end

    test "delete_marked_project/1 deletes the marked_project" do
      marked_project = marked_project_fixture()
      assert {:ok, %MarkedProject{}} = Mentorship.delete_marked_project(marked_project)

      assert_raise Ecto.NoResultsError, fn ->
        Mentorship.get_marked_project!(marked_project.id)
      end
    end

    test "change_marked_project/1 returns a marked_project changeset" do
      marked_project = marked_project_fixture()
      assert %Ecto.Changeset{} = Mentorship.change_marked_project(marked_project)
    end
  end

  describe "modules" do
    alias Ergo.Mentorship.Module

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def module_fixture(attrs \\ %{}) do
      {:ok, module} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Mentorship.create_module()

      module
    end

    test "list_modules/0 returns all modules" do
      module = module_fixture()
      assert Mentorship.list_modules() == [module]
    end

    test "get_module!/1 returns the module with given id" do
      module = module_fixture()
      assert Mentorship.get_module!(module.id) == module
    end

    test "create_module/1 with valid data creates a module" do
      assert {:ok, %Module{} = module} = Mentorship.create_module(@valid_attrs)
      assert module.name == "some name"
    end

    test "create_module/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Mentorship.create_module(@invalid_attrs)
    end

    test "update_module/2 with valid data updates the module" do
      module = module_fixture()
      assert {:ok, %Module{} = module} = Mentorship.update_module(module, @update_attrs)
      assert module.name == "some updated name"
    end

    test "update_module/2 with invalid data returns error changeset" do
      module = module_fixture()
      assert {:error, %Ecto.Changeset{}} = Mentorship.update_module(module, @invalid_attrs)
      assert module == Mentorship.get_module!(module.id)
    end

    test "delete_module/1 deletes the module" do
      module = module_fixture()
      assert {:ok, %Module{}} = Mentorship.delete_module(module)
      assert_raise Ecto.NoResultsError, fn -> Mentorship.get_module!(module.id) end
    end

    test "change_module/1 returns a module changeset" do
      module = module_fixture()
      assert %Ecto.Changeset{} = Mentorship.change_module(module)
    end
  end

  describe "feedback" do
    alias Ergo.Mentorship.Feedback

    @valid_attrs %{feedback: "some feedback", project_section_id: 42}
    @update_attrs %{feedback: "some updated feedback", project_section_id: 43}
    @invalid_attrs %{feedback: nil, project_section_id: nil}

    def feedback_fixture(attrs \\ %{}) do
      {:ok, feedback} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Mentorship.create_feedback()

      feedback
    end

    test "list_feedback/0 returns all feedback" do
      feedback = feedback_fixture()
      assert Mentorship.list_feedback() == [feedback]
    end

    test "get_feedback!/1 returns the feedback with given id" do
      feedback = feedback_fixture()
      assert Mentorship.get_feedback!(feedback.id) == feedback
    end

    test "create_feedback/1 with valid data creates a feedback" do
      assert {:ok, %Feedback{} = feedback} = Mentorship.create_feedback(@valid_attrs)
      assert feedback.feedback == "some feedback"
      assert feedback.project_section_id == 42
    end

    test "create_feedback/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Mentorship.create_feedback(@invalid_attrs)
    end

    test "update_feedback/2 with valid data updates the feedback" do
      feedback = feedback_fixture()
      assert {:ok, %Feedback{} = feedback} = Mentorship.update_feedback(feedback, @update_attrs)
      assert feedback.feedback == "some updated feedback"
      assert feedback.project_section_id == 43
    end

    test "update_feedback/2 with invalid data returns error changeset" do
      feedback = feedback_fixture()
      assert {:error, %Ecto.Changeset{}} = Mentorship.update_feedback(feedback, @invalid_attrs)
      assert feedback == Mentorship.get_feedback!(feedback.id)
    end

    test "delete_feedback/1 deletes the feedback" do
      feedback = feedback_fixture()
      assert {:ok, %Feedback{}} = Mentorship.delete_feedback(feedback)
      assert_raise Ecto.NoResultsError, fn -> Mentorship.get_feedback!(feedback.id) end
    end

    test "change_feedback/1 returns a feedback changeset" do
      feedback = feedback_fixture()
      assert %Ecto.Changeset{} = Mentorship.change_feedback(feedback)
    end
  end
end
