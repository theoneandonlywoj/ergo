defmodule ErgoWeb.CohortControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Admin
  alias Ergo.Admin.Cohort

  @create_attrs %{
    name: "some name"
  }
  @update_attrs %{
    name: "some updated name"
  }
  @invalid_attrs %{name: nil}

  def fixture(:cohort) do
    {:ok, cohort} = Admin.create_cohort(@create_attrs)
    cohort
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all cohorts", %{conn: conn} do
      conn = get(conn, Routes.cohort_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create cohort" do
    test "renders cohort when data is valid", %{conn: conn} do
      conn = post(conn, Routes.cohort_path(conn, :create), cohort: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.cohort_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.cohort_path(conn, :create), cohort: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update cohort" do
    setup [:create_cohort]

    test "renders cohort when data is valid", %{conn: conn, cohort: %Cohort{id: id} = cohort} do
      conn = put(conn, Routes.cohort_path(conn, :update, cohort), cohort: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.cohort_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, cohort: cohort} do
      conn = put(conn, Routes.cohort_path(conn, :update, cohort), cohort: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete cohort" do
    setup [:create_cohort]

    test "deletes chosen cohort", %{conn: conn, cohort: cohort} do
      conn = delete(conn, Routes.cohort_path(conn, :delete, cohort))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.cohort_path(conn, :show, cohort))
      end
    end
  end

  defp create_cohort(_) do
    cohort = fixture(:cohort)
    {:ok, cohort: cohort}
  end
end
