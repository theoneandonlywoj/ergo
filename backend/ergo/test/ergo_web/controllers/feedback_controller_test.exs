defmodule ErgoWeb.FeedbackControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Mentorship
  alias Ergo.Mentorship.Feedback

  @create_attrs %{
    feedback: "some feedback",
    project_section_id: 42
  }
  @update_attrs %{
    feedback: "some updated feedback",
    project_section_id: 43
  }
  @invalid_attrs %{feedback: nil, project_section_id: nil}

  def fixture(:feedback) do
    {:ok, feedback} = Mentorship.create_feedback(@create_attrs)
    feedback
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all feedback", %{conn: conn} do
      conn = get(conn, Routes.feedback_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create feedback" do
    test "renders feedback when data is valid", %{conn: conn} do
      conn = post(conn, Routes.feedback_path(conn, :create), feedback: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.feedback_path(conn, :show, id))

      assert %{
               "id" => id,
               "feedback" => "some feedback",
               "project_section_id" => 42
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.feedback_path(conn, :create), feedback: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update feedback" do
    setup [:create_feedback]

    test "renders feedback when data is valid", %{
      conn: conn,
      feedback: %Feedback{id: id} = feedback
    } do
      conn = put(conn, Routes.feedback_path(conn, :update, feedback), feedback: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.feedback_path(conn, :show, id))

      assert %{
               "id" => id,
               "feedback" => "some updated feedback",
               "project_section_id" => 43
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, feedback: feedback} do
      conn = put(conn, Routes.feedback_path(conn, :update, feedback), feedback: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete feedback" do
    setup [:create_feedback]

    test "deletes chosen feedback", %{conn: conn, feedback: feedback} do
      conn = delete(conn, Routes.feedback_path(conn, :delete, feedback))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.feedback_path(conn, :show, feedback))
      end
    end
  end

  defp create_feedback(_) do
    feedback = fixture(:feedback)
    {:ok, feedback: feedback}
  end
end
