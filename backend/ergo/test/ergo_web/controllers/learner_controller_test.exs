defmodule ErgoWeb.LearnerControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Admin
  alias Ergo.Admin.Learner

  @create_attrs %{
    cohort_id: 42,
    name: "some name"
  }
  @update_attrs %{
    cohort_id: 43,
    name: "some updated name"
  }
  @invalid_attrs %{cohort_id: nil, name: nil}

  def fixture(:learner) do
    {:ok, learner} = Admin.create_learner(@create_attrs)
    learner
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all learners", %{conn: conn} do
      conn = get(conn, Routes.learner_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create learner" do
    test "renders learner when data is valid", %{conn: conn} do
      conn = post(conn, Routes.learner_path(conn, :create), learner: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.learner_path(conn, :show, id))

      assert %{
               "id" => id,
               "cohort_id" => 42,
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.learner_path(conn, :create), learner: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update learner" do
    setup [:create_learner]

    test "renders learner when data is valid", %{conn: conn, learner: %Learner{id: id} = learner} do
      conn = put(conn, Routes.learner_path(conn, :update, learner), learner: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.learner_path(conn, :show, id))

      assert %{
               "id" => id,
               "cohort_id" => 43,
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, learner: learner} do
      conn = put(conn, Routes.learner_path(conn, :update, learner), learner: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete learner" do
    setup [:create_learner]

    test "deletes chosen learner", %{conn: conn, learner: learner} do
      conn = delete(conn, Routes.learner_path(conn, :delete, learner))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.learner_path(conn, :show, learner))
      end
    end
  end

  defp create_learner(_) do
    learner = fixture(:learner)
    {:ok, learner: learner}
  end
end
