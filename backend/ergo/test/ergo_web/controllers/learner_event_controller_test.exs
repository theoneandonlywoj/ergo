defmodule ErgoWeb.LearnerEventControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Admin
  alias Ergo.Admin.LearnerEvent

  @create_attrs %{
    details: "some details",
    learner_id: 42,
    type: "some type"
  }
  @update_attrs %{
    details: "some updated details",
    learner_id: 43,
    type: "some updated type"
  }
  @invalid_attrs %{details: nil, learner_id: nil, type: nil}

  def fixture(:learner_event) do
    {:ok, learner_event} = Admin.create_learner_event(@create_attrs)
    learner_event
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all learner_events", %{conn: conn} do
      conn = get(conn, Routes.learner_event_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create learner_event" do
    test "renders learner_event when data is valid", %{conn: conn} do
      conn = post(conn, Routes.learner_event_path(conn, :create), learner_event: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.learner_event_path(conn, :show, id))

      assert %{
               "id" => id,
               "details" => "some details",
               "learner_id" => 42,
               "type" => "some type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.learner_event_path(conn, :create), learner_event: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update learner_event" do
    setup [:create_learner_event]

    test "renders learner_event when data is valid", %{
      conn: conn,
      learner_event: %LearnerEvent{id: id} = learner_event
    } do
      conn =
        put(conn, Routes.learner_event_path(conn, :update, learner_event),
          learner_event: @update_attrs
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.learner_event_path(conn, :show, id))

      assert %{
               "id" => id,
               "details" => "some updated details",
               "learner_id" => 43,
               "type" => "some updated type"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, learner_event: learner_event} do
      conn =
        put(conn, Routes.learner_event_path(conn, :update, learner_event),
          learner_event: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete learner_event" do
    setup [:create_learner_event]

    test "deletes chosen learner_event", %{conn: conn, learner_event: learner_event} do
      conn = delete(conn, Routes.learner_event_path(conn, :delete, learner_event))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.learner_event_path(conn, :show, learner_event))
      end
    end
  end

  defp create_learner_event(_) do
    learner_event = fixture(:learner_event)
    {:ok, learner_event: learner_event}
  end
end
