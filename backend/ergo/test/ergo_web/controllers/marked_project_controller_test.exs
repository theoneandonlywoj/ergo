defmodule ErgoWeb.MarkedProjectControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Mentorship
  alias Ergo.Mentorship.MarkedProject

  @create_attrs %{
    comments: "some comments",
    grade: "some grade",
    learner: "some learner",
    marker: "some marker",
    module: "some module"
  }
  @update_attrs %{
    comments: "some updated comments",
    grade: "some updated grade",
    learner: "some updated learner",
    marker: "some updated marker",
    module: "some updated module"
  }
  @invalid_attrs %{comments: nil, grade: nil, learner: nil, marker: nil, module: nil}

  def fixture(:marked_project) do
    {:ok, marked_project} = Mentorship.create_marked_project(@create_attrs)
    marked_project
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all marked_projects", %{conn: conn} do
      conn = get(conn, Routes.marked_project_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create marked_project" do
    test "renders marked_project when data is valid", %{conn: conn} do
      conn = post(conn, Routes.marked_project_path(conn, :create), marked_project: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.marked_project_path(conn, :show, id))

      assert %{
               "id" => id,
               "comments" => "some comments",
               "grade" => "some grade",
               "learner" => "some learner",
               "marker" => "some marker",
               "module" => "some module"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.marked_project_path(conn, :create), marked_project: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update marked_project" do
    setup [:create_marked_project]

    test "renders marked_project when data is valid", %{
      conn: conn,
      marked_project: %MarkedProject{id: id} = marked_project
    } do
      conn =
        put(conn, Routes.marked_project_path(conn, :update, marked_project),
          marked_project: @update_attrs
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.marked_project_path(conn, :show, id))

      assert %{
               "id" => id,
               "comments" => "some updated comments",
               "grade" => "some updated grade",
               "learner" => "some updated learner",
               "marker" => "some updated marker",
               "module" => "some updated module"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, marked_project: marked_project} do
      conn =
        put(conn, Routes.marked_project_path(conn, :update, marked_project),
          marked_project: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete marked_project" do
    setup [:create_marked_project]

    test "deletes chosen marked_project", %{conn: conn, marked_project: marked_project} do
      conn = delete(conn, Routes.marked_project_path(conn, :delete, marked_project))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.marked_project_path(conn, :show, marked_project))
      end
    end
  end

  defp create_marked_project(_) do
    marked_project = fixture(:marked_project)
    {:ok, marked_project: marked_project}
  end
end
