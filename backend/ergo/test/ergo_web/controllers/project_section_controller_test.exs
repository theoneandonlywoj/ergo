defmodule ErgoWeb.ProjectSectionControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Mentorship
  alias Ergo.Mentorship.ProjectSection

  @create_attrs %{
    name: "some name",
    project_id: 42
  }
  @update_attrs %{
    name: "some updated name",
    project_id: 43
  }
  @invalid_attrs %{name: nil, project_id: nil}

  def fixture(:project_section) do
    {:ok, project_section} = Mentorship.create_project_section(@create_attrs)
    project_section
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all project_sections", %{conn: conn} do
      conn = get(conn, Routes.project_section_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create project_section" do
    test "renders project_section when data is valid", %{conn: conn} do
      conn =
        post(conn, Routes.project_section_path(conn, :create), project_section: @create_attrs)

      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.project_section_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "some name",
               "project_id" => 42
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn =
        post(conn, Routes.project_section_path(conn, :create), project_section: @invalid_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update project_section" do
    setup [:create_project_section]

    test "renders project_section when data is valid", %{
      conn: conn,
      project_section: %ProjectSection{id: id} = project_section
    } do
      conn =
        put(conn, Routes.project_section_path(conn, :update, project_section),
          project_section: @update_attrs
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.project_section_path(conn, :show, id))

      assert %{
               "id" => id,
               "name" => "some updated name",
               "project_id" => 43
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, project_section: project_section} do
      conn =
        put(conn, Routes.project_section_path(conn, :update, project_section),
          project_section: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete project_section" do
    setup [:create_project_section]

    test "deletes chosen project_section", %{conn: conn, project_section: project_section} do
      conn = delete(conn, Routes.project_section_path(conn, :delete, project_section))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.project_section_path(conn, :show, project_section))
      end
    end
  end

  defp create_project_section(_) do
    project_section = fixture(:project_section)
    {:ok, project_section: project_section}
  end
end
