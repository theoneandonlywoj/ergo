defmodule ErgoWeb.UserCohortSubscriptionControllerTest do
  use ErgoWeb.ConnCase

  alias Ergo.Admin
  alias Ergo.Admin.UserCohortSubscription

  @create_attrs %{
    cohort_id: 42,
    user_id: 42
  }
  @update_attrs %{
    cohort_id: 43,
    user_id: 43
  }
  @invalid_attrs %{cohort_id: nil, user_id: nil}

  def fixture(:user_cohort_subscription) do
    {:ok, user_cohort_subscription} = Admin.create_user_cohort_subscription(@create_attrs)
    user_cohort_subscription
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all user_cohort_subscriptions", %{conn: conn} do
      conn = get(conn, Routes.user_cohort_subscription_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create user_cohort_subscription" do
    test "renders user_cohort_subscription when data is valid", %{conn: conn} do
      conn =
        post(conn, Routes.user_cohort_subscription_path(conn, :create),
          user_cohort_subscription: @create_attrs
        )

      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.user_cohort_subscription_path(conn, :show, id))

      assert %{
               "id" => id,
               "cohort_id" => 42,
               "user_id" => 42
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn =
        post(conn, Routes.user_cohort_subscription_path(conn, :create),
          user_cohort_subscription: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update user_cohort_subscription" do
    setup [:create_user_cohort_subscription]

    test "renders user_cohort_subscription when data is valid", %{
      conn: conn,
      user_cohort_subscription: %UserCohortSubscription{id: id} = user_cohort_subscription
    } do
      conn =
        put(conn, Routes.user_cohort_subscription_path(conn, :update, user_cohort_subscription),
          user_cohort_subscription: @update_attrs
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.user_cohort_subscription_path(conn, :show, id))

      assert %{
               "id" => id,
               "cohort_id" => 43,
               "user_id" => 43
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{
      conn: conn,
      user_cohort_subscription: user_cohort_subscription
    } do
      conn =
        put(conn, Routes.user_cohort_subscription_path(conn, :update, user_cohort_subscription),
          user_cohort_subscription: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete user_cohort_subscription" do
    setup [:create_user_cohort_subscription]

    test "deletes chosen user_cohort_subscription", %{
      conn: conn,
      user_cohort_subscription: user_cohort_subscription
    } do
      conn =
        delete(
          conn,
          Routes.user_cohort_subscription_path(conn, :delete, user_cohort_subscription)
        )

      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.user_cohort_subscription_path(conn, :show, user_cohort_subscription))
      end
    end
  end

  defp create_user_cohort_subscription(_) do
    user_cohort_subscription = fixture(:user_cohort_subscription)
    {:ok, user_cohort_subscription: user_cohort_subscription}
  end
end
