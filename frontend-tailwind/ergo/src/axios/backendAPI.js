import axios from "axios";

const backendAPI = axios.create({
  baseURL: `http://${process.env.VUE_APP_BACKEND_ENDPOINT}/api`
});

export default backendAPI;
