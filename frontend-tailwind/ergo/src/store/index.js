import Vue from "vue";
import Vuex from "vuex";
import learnerRecordsDialog from "./learnerRecordsDialog";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    learnerRecordsDialog
  }
});
