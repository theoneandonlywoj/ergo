export function showLearnerRecordsDialogAction({ commit }) {
  commit("showLearnerRecordsDialogMutation");
}

export function hideLearnerRecordsDialogAction({ commit }) {
  commit("hideLearnerRecordsDialogMutation");
}
