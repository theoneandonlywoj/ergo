export function learnerRecordsDialogGetter(state) {
  return state.learnerRecordsDialog;
}

export function learnerIdGetter(state) {
  return state.learnerId;
}

export function addLearnerEventFormGetter(state) {
  return state.addLearnerEventForm;
}
