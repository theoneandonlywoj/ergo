export function showLearnerRecordsDialogMutation(state) {
  state.learnerRecordsDialog = true;
}

export function hideLearnerRecordsDialogMutation(state) {
  state.learnerRecordsDialog = false;
}

export function setLearnerIdMutation(state, value) {
  state.learnerId = value;
}

export function showAddLearnerEventFormMutation(state) {
  state.addLearnerEventForm = true;
}

export function hideAddLearnerEventFormMutation(state) {
  state.addLearnerEventForm = false;
}
