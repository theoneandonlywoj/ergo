import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import LoggedInFrame from "../views/LoggedInFrame.vue";
import MarkingScheme from "../views/MarkingScheme.vue";
import RenderedMarkingScheme from "../views/RenderedMarkingScheme.vue";
import LearnersRecordsDashboard from "../components/LearnersRecordsDashboard.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/loggedIn",
    name: "loggedIn",
    component: LoggedInFrame,
    children: [
      {
        path: "/",
        name: "LearnersRecordsDashboard",
        component: LearnersRecordsDashboard
      }
    ]
  },
  {
    path: "/marking-scheme",
    name: "markingScheme",
    component: MarkingScheme
  },
  {
    path: "/rendered",
    name: "renderedMarkingScheme",
    component: RenderedMarkingScheme
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
