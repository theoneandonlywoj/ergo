import Vue from "vue";
import Vuex from "vuex";
import MarkingScheme from "./markingScheme";
import NavigationDrawer from "./navigationDrawer";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    MarkingScheme,
    NavigationDrawer
  }
});
