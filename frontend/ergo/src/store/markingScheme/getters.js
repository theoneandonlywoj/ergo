export function modeGetter(state) {
  return state.mode;
}

export function cohortGetter(state) {
  return state.cohort;
}

export function learnerGetter(state) {
  return state.learner;
}

export function moduleGetter(state) {
  return state.module;
}

export function markerGetter(state) {
  return state.marker;
}

export function gradeGetter(state) {
  return state.grade;
}

export function dateGetter(state) {
  return state.date;
}

export function commentsGetter(state) {
  return state.comments;
}

export function tableDataGetter(state) {
  return state.tableData;
}

export function selectedWellDoneGetter(state) {
  return state.selectedWellDone;
}

export function selectedNeedsCorrectionsGetter(state) {
  return state.selectedNeedsCorrections;
}

export function showAddRequirementDialogGetter(state) {
  return state.showDialog && state.showDialogMode === "newRequirement";
}

export function markingSchemeGetter(state) {
  return state;
}
