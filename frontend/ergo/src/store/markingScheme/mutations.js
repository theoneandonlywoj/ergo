export function setModeMutation(state, value) {
  state.mode = value;
}

export function setLearnerMutation(state, value) {
  state.learner = value;
}

export function setCohortMutation(state, value) {
  state.cohort = value;
}

export function setModuleMutation(state, value) {
  state.module = value;
}

export function setMarkerMutation(state, value) {
  state.marker = value;
}

export function setGradeMutation(state, value) {
  state.grade = value;
}

export function setDateMutation(state, value) {
  state.date = value;
}

export function setCommentsMutation(state, value) {
  state.comments = value;
}

export function setTableDataMutation(state, value) {
  state.tableData = value;
}

export function setSelectedWellDoneMutation(state, value) {
  state.selectedWellDone = value;
}

export function toggleSelectedWellDoneMutation(state, item) {
  const itemInSelectedNeedsCorrections = state.selectedNeedsCorrections.filter(
    tableItem => tableItem.id === item.id
  );
  if (itemInSelectedNeedsCorrections.length > 0) {
    state.selectedNeedsCorrections = state.selectedNeedsCorrections.filter(
      itemInTable => itemInTable.id !== item.id
    );
  }
  const itemInSelectedWellDone = state.selectedWellDone.filter(
    tableItem => tableItem.id === item.id
  );
  if (itemInSelectedWellDone.length > 0) {
    state.selectedWellDone = state.selectedWellDone.filter(
      itemInTable => itemInTable.id !== item.id
    );
  } else {
    state.selectedWellDone = state.selectedWellDone.concat([item]);
  }
}

export function toggleNeedsCorrectionsMutation(state, item) {
  const itemInSelectedWellDone = state.selectedWellDone.filter(
    tableItem => tableItem.id === item.id
  );
  if (itemInSelectedWellDone.length > 0) {
    state.selectedWellDone = state.selectedWellDone.filter(
      itemInTable => itemInTable.id !== item.id
    );
  }
  const itemInSelectedNeedsCorrections = state.selectedNeedsCorrections.filter(
    tableItem => tableItem.id === item.id
  );
  if (itemInSelectedNeedsCorrections.length > 0) {
    state.selectedNeedsCorrections = state.selectedNeedsCorrections.filter(
      itemInTable => itemInTable.id !== item.id
    );
  } else {
    state.selectedNeedsCorrections = state.selectedNeedsCorrections.concat([
      item
    ]);
  }
}

export function setSelectedNeedsCorrectionsMutation(state, value) {
  state.selectedNeedsCorrections = value;
}

export function showAddRequirementDialog(state) {
  state.showDialog = true;
  state.showDialogMode = "newRequirement";
}

export function hideAddRequirementDialog(state) {
  state.showDialog = false;
  state.showDialogMode = "";
}
