export default {
  mode: "marking",
  // mode: "pre-sign",
  cohort: "",
  learner: "",
  module: "",
  marker: "",
  grade: "",
  date: "",
  comments: "",
  search: "",
  tableData: [],
  selectedWellDone: [],
  selectedNeedsCorrections: [],
  showDialog: false,
  showDialogMode: ""
};
