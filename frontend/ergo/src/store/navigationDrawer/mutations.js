export function showNavigationDrawerMutation(state) {
  state.showNavigationDrawerState = true;
}

export function hideNavigationDrawerMutation(state) {
  state.showNavigationDrawerState = false;
}

export function toggleNavigationDrawerMutation(state) {
  state.showNavigationDrawerState = !state.showNavigationDrawerState;
}
